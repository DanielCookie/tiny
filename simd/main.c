#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <immintrin.h>

#include "darr.h"
dyn_arr_methods_for_type(f, float)

const size_t Floats_in_Register = 8;

/* not working, why? */
da_t simd_add(da_t a, da_t b) {
	da_t res = da_new(a.len, sizeof(float));
	// using avx, 256bit registers or 8 32bit floating point variables
	// how many groups of variables of size 8 can we process with simd

	const size_t avx_register_sz = Floats_in_Register * sizeof(float);
	const size_t vectorisable_samples = a.len / Floats_in_Register;
	const size_t non_vectorisable_samples = a.len % Floats_in_Register;
	const size_t floats_after_vectorised = vectorisable_samples * Floats_in_Register;

	printf("%d elements\n", res.len);
	printf("vectorisable_samples: %d\n", vectorisable_samples);
	printf("non-vectorisable_samples: %d\n", non_vectorisable_samples);

	size_t i;
	for (i = 0; i < vectorisable_samples; i++) {
		const __m256 intermediate_sum = _mm256_add_ps(
			_mm256_loadu_ps(a.data + i * avx_register_sz),
			_mm256_loadu_ps(b.data + i * avx_register_sz));
		_mm256_storeu_ps(res.data + i * avx_register_sz, intermediate_sum);
	}

	for (i = 0; i < non_vectorisable_samples; i++) {
		da_put_f(&res, floats_after_vectorised + i,
			da_get_f(&a, floats_after_vectorised + i) +
			da_get_f(&b, floats_after_vectorised + i));
	}

	return res;
}

int main() {
	da_t a = da_new(17, sizeof(float));

	for (size_t i = 0; i < a.len; i++) {
		da_put_f(&a, i, 1.0);
	}

	da_t result = simd_add(a, a);

	for (size_t i = 0; i < result.len-1; i++) {
		printf("%.0f ", da_get_f(&result, i));
	}
	printf("%.0f\n", da_last_f(&result));

	da_terminate(&result);
	da_terminate(&a);
	return 0;
}
