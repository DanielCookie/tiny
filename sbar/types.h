// Numbers
typedef char s8;
typedef unsigned char u8;
typedef short s16;
typedef unsigned short u16;
typedef int s32;
typedef unsigned int u32;
typedef long long s64;
typedef unsigned long long u64;

// Booleans
typedef u8 boole;
const boole On = 1;
const boole Off = 0;

// Bitsets
typedef u8 bitset_t;
typedef bitset_t bitset_member_t;

#define bitset_on(set, member)\
	set |= (bitset_member_t)1 << (bitset_member_t)member

#define bitset_off(set, member)\
	set &= ~((bitset_member_t)1 << (bitset_member_t)member)

#define bitset_flip(set, member)\
	set ^= (bitset_member_t)1 << (bitset_member_t)member

#define bitset_toggle(set, member, boolean)\
	set =\
		(set & ~((bitset_member_t)1 << (bitset_member_t)member))\
		^ ((bitset_member_t)boolean << (bitset_member_t)member)

#define bitset_has(set, member)\
	(u8)(\
		(bitset_t)set & ((bitset_member_t)1 << (bitset_member_t)member)\
	)

const bitset_t Empty_Bitset = 0;

// Strings
typedef struct {
	char *s;
	u32 len;
} str_t;

#define str(c) ((str_t) {.s = (char *)(c), .len = (sizeof(c)-1)})
