M = {}

M.constants = {
	Bar_Cursor_Width   = {int64  = 6}, -- absolute amount
	Bar_Height         = {int64  = 10}, -- absolute amount
	Bar_Width_Fraction = {double = 0.95}, -- relative amount; range: 0-1; Width of bar as fraction of window width
	Bar_Border_Width   = {int64  = 3},

	Bar_Bottom_Padding_Fraction  = {double = 0.03}, -- relative_amount; range: 0-1; Padding below bar as fraction of video height
	Bar_Status_Vertical_Offset   = {int64  = 22}, -- absolute amount; Distance offset above the bar where the status text is drawn
	Bar_Status_Horizontal_Offset = {int64  = 2}, -- absolute amount; Distance offset from the left end of the bar where the status text is drawn

	Text_Border_Width = {int64 = 2},

	Info_Overlay_Line_Height = {int64 = 20},

	Time_Pos_and_Duration_Separator     = {string = " / "},
	Left_Justified_Hover_Pos_Indicator  = {string = "~ "},
	Right_Justified_Hover_Pos_Indicator = {string = " ~"},
	Seeking_Status                      = {string = " •••"},
	Paused_for_Cache_Status             = {string = " <buffering>"},
	Idling_Status                       = {string = " <waiting>"},

	Yank_System_Cmd  = {string = "wl-copy -n"},
	Paste_System_Cmd = {string = "wl-paste"},

	Bar_Hidden_Def        = {boolean = false},
	Bar_Status_Hidden_Def = {boolean = true},

	Fg_Col     = {color = "EBA28B"},
	Icon_Col   = {color = "EBA28B"},
	Border_Col = {color = "161616"},
	Bar_Col    = {color = "0E0E0E"},
	Trough_Col = {color = "040404"},
	Cursor_Col = {color = "EBA28B"},

	Chapter_Marker_Radius       = {int64 = 3},
	Chapter_Marker_Border_Width = {int64 = 1},

	Chapter_Marker_Col                = {color = "282A58"},
	Chapter_Marker_Border_Col         = {color = "8FBCBB"},
	Current_Chapter_Marker_Col        = {color = "EBCB8B"},
	Current_Chapter_Marker_Border_Col = {color = "EBCB8B"},

	-- 0 -> insert-next
	-- 1 -> insert-next-play
	-- 2 -> append
	-- 3 -> append-play
	Paste_Filepath_Behaviour = {macro = 1};
}

return M
