local cfg = require("config")

for k,v in pairs(cfg.constants) do
	if v.macro then
		print("#define "..k.." ("..v.macro..")");
	elseif v.int64 then
		print("const s64 "..k.." = "..v.int64..";");
	elseif v.double then
		print("const double "..k.." = "..v.double..";");
	elseif v.string then
		print("const char "..k.."[] = \""..v.string.."\";");
	elseif v.boolean ~= nil then
		print("const boole "..k.." = "..(v.boolean and "On" or "Off")..";");
	elseif v.color then
		local convert = function(color)
			local r = color:sub(1,2)
			local g = color:sub(3,4)
			local b = color:sub(5,6)
			return b..g..r
		end
		print("const str_t "..k.." = str(\""..convert(v.color).."\");");
	else
		assert(false);
	end
end
