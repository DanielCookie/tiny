# sbar

Simple C plugin for the mpv video player.

## Features

- Interactable bar the displays the current playback progress (toggleable visibility).
- Chapter markers shown along progress bar.
- Status line shown above bar (toggleable visibility).
- Switch between two status lines: one with playback progress and the other with video title and position in playlist.
- Pause indicator icon shown when playback is paused (upper right corner).
- Mute indicator icon shown when sound is muted (upper right corner).
- Heads-up text shown when changing the loop status of current file.
- Copy the current position in the file into the system clipboard.
- Set position in the file by reading it from the system clipboard.
- Copy the file path into the system clipboard.
- Add a filepath in the system clipboard to the playlist.
- Single click to pause (requires setting keybinding).
- Minimalistic information overlay that displays basic data about the audio and video.

See suggested keybindings below.

## Building

- Requires `lua` and `samurai`/`ninja`.
- Configured before compilation through `config.lua`.

Run `samu` and it should build the config header file followed by `sbar.so`.

## Installation

Modify the install location in `config.ninja` and run `samu i`.

## Usage & mpv configuration

To use all features, requires the following mpv configuration:

**In** `mpv.conf`:
- It works better if used in conjunction with `osc=no` and `osd-level=0`

**In** `input.conf`:

**Suggested keybindings**:
```
s script-message-to sbar toggle-bar
o script-message-to sbar toggle-bar-status
O script-message-to sbar swap-bar-status
y script-message-to sbar yank-current-pos
p script-message-to sbar set-pos-from-clipboard
Y script-message-to sbar yank-file-path
P script-message-to sbar add-file-from-clipboard
i script-message-to sbar show-info
mbtn_left script-message-to sbar sent-return # needed to set the position when clicking on bar; pauses if clicked elsewhere.
```
