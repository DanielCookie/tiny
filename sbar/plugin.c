#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <mpv/client.h>

#include "types.h"
#include "config.h"

#define Max_Number_Len 17
#define Builder_Cap 4096

typedef str_t builder_t;

typedef void (*callback_f) (mpv_handle *ctx, mpv_event_property *prop);
typedef void (draw_bar_status_f) (mpv_handle *ctx, const s64 x_start, const s64 y_start);

enum Osd_Position: u8 {
	Position_Abs  = 0,
	South_West    = 1,
	South_Middle  = 2,
	South_East    = 3,
	Centre_West   = 4,
	Centre_Middle = 5,
	Centre_East   = 6,
	North_West    = 7,
	North_Middle  = 8,
	North_East    = 9,
};

enum Watcher: u64 {
	Pause_Watcher = 99,
	Mute_Watcher,
	Loop_File_Watcher,
	Osd_Dim_Watcher,
	Vo_Ready_Watcher,
	Time_Pos_Watcher,
	Duration_Watcher,
	Speed_Watcher,
	Mouse_Watcher,
	Idle_Watcher,
	Cache_Pause_Watcher,
	Chapters_Watcher,
	Chapter_Watcher,
	Playlist_Len_Watcher,
	Title_Watcher,
	Bitrate_Watcher,
};

enum Overlays: u64 {
	Pause_Overlay = 99,
	Mute_Overlay,
	Bar_Overlay,
	Chapter_Overlay,
	Bar_Status_Overlay,
	Hover_Pos_Overlay,
	Info_Overlay,
};

enum Playback_Member: u8 {
	Paused_m,
	Seeking_m,
	Cache_Paused_m,
	Idle_m,
	Muted_m,
};

enum Bar_Member: u8 {
	Hidden_m,
	Status_Hidden_m,
	Hovering_m,
};

const char Overlay_Cmd_Ass[]  = "ass-events";
const char Overlay_Cmd_None[] = "none";

static void num2str(s64 num, str_t *buf);

static void bldr_zero(builder_t *bldr);
static void bldr_add_s(builder_t *bldr, str_t str);
static void bldr_add_c(builder_t *bldr, char ch);
static void bldr_add_n(builder_t *bldr, s64 num);

static void format_timestamp(s64 time);
static void format_mem_size(s64 bytes);

static void show_text(mpv_handle *ctx, const char *text);

static void draw_shape_init(enum Osd_Position osd_pos, str_t color, s64 border_w);
static void draw_shape_finish();
static void draw_rect(s64 x, s64 y, s64 wi, s64 ht);
static void draw_diamond(s64 x, s64 y, s64 radius);
static void draw_text_init(s64 x, s64 y, enum Osd_Position osd_pos, str_t color, s64 border_w);
static void draw_change_color(str_t color);
static void draw_change_border_color(str_t color);
static void draw_commit(mpv_handle *ctx, enum Overlays overlay, char *overlay_cmd);
static void draw_pause_indicator(mpv_handle *ctx);
static void draw_mute_indicator(mpv_handle *ctx);
static void draw_hover_pos(mpv_handle *ctx, s64 mouse_x, s64 mouse_y);
static void draw_bar(mpv_handle *ctx);
static void draw_bar_status_playback(mpv_handle *ctx, const s64 x_start, const s64 y_start);
static void draw_bar_status_semantic(mpv_handle *ctx, const s64 x_start, const s64 y_start);
static void draw_bar_status_chapter(mpv_handle *ctx, const s64 x_start, const s64 y_start);
static void draw_chapter_markers(mpv_handle *ctx, s64 x_start, s64 y_start, s64 width, s64 height);

static void hide_bar(mpv_handle *ctx);
static void hide_bar_status(mpv_handle *ctx);
static void proc_return(mpv_handle *ctx);
static void yank_time_pos(mpv_handle *ctx);
static void yank_file_path(mpv_handle *ctx);
static void paste_time_pos(mpv_handle *ctx);
static void paste_filepath(mpv_handle *ctx);
static void show_info(mpv_handle *ctx);
static void swap_bar_status(mpv_handle *ctx);
static void draw_info(mpv_handle *ctx);

static void osd_dim_callback     (mpv_handle *ctx, mpv_event_property *prop);
static void pause_callback       (mpv_handle *ctx, mpv_event_property *prop);
static void mute_callback        (mpv_handle *ctx, mpv_event_property *prop);
static void loop_file_callback   (mpv_handle *ctx, mpv_event_property *prop);
static void time_pos_callback    (mpv_handle *ctx, mpv_event_property *prop);
static void duration_callback    (mpv_handle *ctx, mpv_event_property *prop);
static void speed_callback       (mpv_handle *ctx, mpv_event_property *prop);
static void mouse_callback       (mpv_handle *ctx, mpv_event_property *prop);
// static void demuxer_state_callback      (mpv_handle *ctx, mpv_event_property *prop);
static void idle_callback        (mpv_handle *ctx, mpv_event_property *prop);
static void cache_pause_callback (mpv_handle *ctx, mpv_event_property *prop);
static void chapters_callback    (mpv_handle *ctx, mpv_event_property *prop);
static void chapter_callback     (mpv_handle *ctx, mpv_event_property *prop);
static void playlist_len_callback(mpv_handle *ctx, mpv_event_property *prop);
static void media_title_callback (mpv_handle *ctx, mpv_event_property *prop);
static void bitrate_callback     (mpv_handle *ctx, mpv_event_property *prop);
static void vo_ready_callback    (mpv_handle *ctx, mpv_event_property *prop);

static draw_bar_status_f *Draw_Bar_Status_f[3] = {
	draw_bar_status_playback,
	draw_bar_status_semantic,
	draw_bar_status_chapter,
};

enum u8 {
	Draw_Bar_Status_Playback,
	Draw_Bar_Status_Semantic,
	Draw_Bar_Status_Chapter,
};

typedef struct {
	builder_t bldr;
	s64 osd_w, osd_h;
	u32 ft_size, ft_border_wi;
	bitset_t playback, bar;
	boole show_info;
	s64 time_pos, duration, hover_pos;
	s64 chapter_count, chapter;
	mpv_node *chapters;
	double speed;
	u8 bar_status_f_id;
	s64 playlist_len;
	str_t media_title;
} state_t;

state_t all = {
	.bldr = {
		.s = (char[Builder_Cap]){0},
		.len = 0,
	},
	.osd_w = 0, .osd_h = 0,
	.ft_size = 0, .ft_border_wi = 0,
	.playback = Empty_Bitset,
	.bar = (Bar_Hidden_Def << Hidden_m) | (Bar_Status_Hidden_Def << Status_Hidden_m),
	.show_info = Off,
	.chapter_count = -1, .chapter = -1,
	.chapters = NULL,
	.time_pos = 0, .duration = 0, .hover_pos = 0,
	.speed = 0.0,
	.bar_status_f_id = Draw_Bar_Status_Playback,
	.playlist_len = 0,
	.media_title = {0},
};

void bldr_zero(builder_t *bldr) {
	for (u32 i = 0; i < bldr->len; i++)
		bldr->s[i] = '\0';
	bldr->len = 0;
}

void bldr_add_s(builder_t *bldr, str_t str) {
	assert(!(Builder_Cap < bldr->len + str.len));

	for (u32 i = 0; i < str.len; i++)
		bldr->s[bldr->len + i] = str.s[i];

	bldr->len += str.len; };  void bldr_add_c(builder_t *bldr, char ch) {
	assert(!(Builder_Cap < bldr->len + 1));

	bldr->s[bldr->len] = ch;
	bldr->len += 1;
}

void bldr_add_n(builder_t *bldr, s64 num) {
	char raw[Max_Number_Len] = {0};
	str_t buf = str(raw);
	num2str(num, &buf);
	bldr_add_s(bldr, buf);
};

void num2str(s64 num, str_t *buf) {
	assert(buf->len <= Max_Number_Len);
	u8 i = 0;
	u8 start = 0;
	s8 sign = 1;
	if (num < 0) {
		start = 1;
		sign = -1;
		buf->s[0] = '-';
	}
	for (u32 rem = (s32)sign * num; rem != 0; rem /= 10) {
		const u32 digit = rem % 10;
		const u32 ascii_digit = digit + 48;
		buf->s[buf->len - 1 - i] = ascii_digit;
		i += 1;
	}
	const u8 digits = i;
	for (i = 0; i < digits; i++) {
		buf->s[start + i] = buf->s[buf->len - digits + i];
	} i += start;
	for (; i < buf->len; i++) {
		buf->s[i] = '\0';
	}
	buf->len = start + digits;
}

void draw_shape_init(enum Osd_Position osd_pos, str_t color, s64 border_w)
{
	if (osd_pos == Position_Abs) {
		const char pos[] = "{\\pos(0,0)\\an7}";
		bldr_add_s(&all.bldr, str(pos));
	}
	else {
		const char pos[] = "{\\an";
		bldr_add_s(&all.bldr, str(pos));
		bldr_add_c(&all.bldr, osd_pos + 48);
		bldr_add_c(&all.bldr, '}');
	}

	const char decor[] = "{\\shad0\\bord";
	bldr_add_s(&all.bldr, str(decor));
	bldr_add_c(&all.bldr, border_w + 48);
	bldr_add_c(&all.bldr, '}');

	if (0 < border_w) {
		const char border_color_cmd[] = "{\\3c&H";
		bldr_add_s(&all.bldr, str(border_color_cmd));
		bldr_add_s(&all.bldr, Border_Col);
		bldr_add_c(&all.bldr, '&');
		bldr_add_c(&all.bldr, '}');
	}

	const char color_cmd[] = "{\\c&H";
	bldr_add_s(&all.bldr, str(color_cmd));
	bldr_add_s(&all.bldr, color);
	bldr_add_c(&all.bldr, '&');
	bldr_add_c(&all.bldr, '}');

	const char open_draw[] = "{\\p1}";
	bldr_add_s(&all.bldr, str(open_draw));
}

void draw_shape_finish() {
	const char close_draw[] = "{\\p0}";
	bldr_add_s(&all.bldr, str(close_draw));
};

void draw_text_init(s64 x, s64 y, enum Osd_Position osd_pos, str_t color, s64 border_w) {
	const char numpad_pos[] = "{\\an";
	bldr_add_s(&all.bldr, str(numpad_pos));
	bldr_add_c(&all.bldr, osd_pos == Position_Abs ? 7 : osd_pos + 48);
	const char pos[] = "\\pos(";
	bldr_add_s(&all.bldr, str(pos));
	bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ',');
	bldr_add_n(&all.bldr, y);
	bldr_add_c(&all.bldr, ')');
	bldr_add_c(&all.bldr, '}');

	const char font_size[] = "{\\fs";
	bldr_add_s(&all.bldr, str(font_size));
	bldr_add_n(&all.bldr, 20);
	bldr_add_c(&all.bldr, '}');

	const char decor[] = "{\\shad0\\bord";
	bldr_add_s(&all.bldr, str(decor));
	bldr_add_c(&all.bldr, border_w + 48);
	bldr_add_c(&all.bldr, '}');

	if (0 < border_w) {
		const char border_color_cmd[] = "{\\3c&H";
		bldr_add_s(&all.bldr, str(border_color_cmd));
		bldr_add_s(&all.bldr, Border_Col);
		bldr_add_c(&all.bldr, '&');
		bldr_add_c(&all.bldr, '}');
	}

	const char color_cmd[] = "{\\c&H";
	bldr_add_s(&all.bldr, str(color_cmd));
	bldr_add_s(&all.bldr, color);
	bldr_add_c(&all.bldr, '&');
	bldr_add_c(&all.bldr, '}');
}

void draw_change_color(str_t color) {
	const char pre[] = "{\\c&H";
	const char post[] = "&}";
	bldr_add_s(&all.bldr, str(pre));
	bldr_add_s(&all.bldr, color);
	bldr_add_s(&all.bldr, str(post));
}

void draw_change_border_color(str_t color) {
	const char pre[] = "{\\3c&H";
	const char post[] = "&}";
	bldr_add_s(&all.bldr, str(pre));
	bldr_add_s(&all.bldr, color);
	bldr_add_s(&all.bldr, str(post));
}

void draw_rect(s64 x, s64 y, s64 wi, s64 ht) {
	bldr_add_c(&all.bldr, 'm');
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);

	bldr_add_c(&all.bldr, ' '); bldr_add_c(&all.bldr, 'l');
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x + wi);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x + wi);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y + ht);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y + ht);
}

void draw_diamond(s64 x, s64 y, s64 radius) {
	bldr_add_c(&all.bldr, 'm');
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);

	bldr_add_c(&all.bldr, ' '); bldr_add_c(&all.bldr, 'l');
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x + radius);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y - radius);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x + 2 * radius);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x + radius);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y + radius);
}

void draw_commit(mpv_handle *ctx, enum Overlays overlay, char *overlay_cmd) {
	const int num_args = 6;
	const mpv_node *args = (mpv_node[]) {
		{.format = MPV_FORMAT_STRING, .u.string = "osd-overlay"},
		{.format = MPV_FORMAT_INT64,  .u.int64  = overlay},
		{.format = MPV_FORMAT_STRING, .u.string = overlay_cmd},
		{.format = MPV_FORMAT_STRING, .u.string = all.bldr.s},
		{.format = MPV_FORMAT_INT64,  .u.int64  = all.osd_w},
		{.format = MPV_FORMAT_INT64,  .u.int64  = all.osd_h},
	};
	const mpv_node_list list = {
		.num = num_args,
		.values = (mpv_node *)args,
		.keys = NULL,
	};
	mpv_node input = {
		.u.list = (mpv_node_list *)&list,
		.format = MPV_FORMAT_NODE_ARRAY,
	};
	mpv_node result;
	mpv_command_node(ctx, &input, &result);
};

void show_text(mpv_handle *ctx, const char *text) {
	const int num_args = 2;
	const mpv_node *args = (mpv_node[]) {
		{.format = MPV_FORMAT_STRING, .u.string = "show-text"},
		{.format = MPV_FORMAT_STRING, .u.string = (char *)text},
	};
	const mpv_node_list list = {
		.num = num_args,
		.values = (mpv_node *)args,
		.keys = NULL,
	};
	mpv_node input = {
		.u.list = (mpv_node_list *)&list,
		.format = MPV_FORMAT_NODE_ARRAY,
	};
	mpv_node result;
	mpv_command_node(ctx, &input, &result);
};

void format_timestamp(s64 time) {
	const s64 secs = time % 60;
	s64 minutes = time / 60;
	const s64 hours = minutes / 60;
	minutes %= 60;
	if (0 < hours) {
		bldr_add_n(&all.bldr, hours);
		bldr_add_c(&all.bldr, ':');
		if (0 < minutes) {
			if (minutes < 10) bldr_add_c(&all.bldr, '0');
			bldr_add_n(&all.bldr, minutes);
			bldr_add_c(&all.bldr, ':');
		}
		else {
			bldr_add_s(&all.bldr, str("00:"));
		}
	}
	else if (0 < minutes) {
		if (minutes < 10) bldr_add_c(&all.bldr, '0');
		bldr_add_n(&all.bldr, minutes);
		bldr_add_c(&all.bldr, ':');
	}
	if (0 < secs) {
		if (secs < 10) bldr_add_c(&all.bldr, '0');
		bldr_add_n(&all.bldr, secs);
	}
	else {
		bldr_add_s(&all.bldr, str("00"));
	}
}

void draw_pause_indicator(mpv_handle *ctx) {
	const s64 y_bounds = all.osd_h;

	bldr_zero(&all.bldr);

	if (!bitset_has(all.playback, Paused_m)) {
		draw_commit(ctx, Pause_Overlay, (char *)Overlay_Cmd_None);
		return;
	}

	const s64 right_margin = 8;
	const s64 x_start = -(right_margin + 0.005 * y_bounds);
	const s64 x_start_ = x_start + 0.025 * y_bounds;
	const s64 y_start = 1;
	const s64 bar_ht = 0.04 * y_bounds;
	const s64 bar_wi = 0.25 * bar_ht;
	const s64 border = 0.006 * y_bounds;
	draw_shape_init(North_East, Icon_Col, border);
	draw_rect(x_start,  y_start, bar_wi, bar_ht);
	draw_rect(x_start_, y_start, bar_wi, bar_ht);
	draw_shape_finish();
	draw_commit(ctx, Pause_Overlay, (char *)Overlay_Cmd_Ass);
}

void draw_mute_indicator(mpv_handle *ctx) {
	const s64 y_bounds = all.osd_h;

	bldr_zero(&all.bldr);

	if (!bitset_has(all.playback, Muted_m)) {
		draw_commit(ctx, Mute_Overlay, (char *)Overlay_Cmd_None);
		return;
	}

	const double start_no_pause = 0.005;
	const double start_pause = 0.07;

	double right_margin = 8;
	right_margin += bitset_has(all.playback, Paused_m) ? start_pause * y_bounds : start_no_pause * y_bounds;

	const s64 handle_ht = 0.017 * y_bounds;
	const s64 handle_wi = 0.7 * handle_ht;
	const s64 mouth_wi  = 1.1 * handle_ht;
	const s64 mouth_ht  = 2.2 * handle_ht;
	const s64 x_start = -right_margin;
	const s64 y_start = 0.015 * y_bounds;
	const s64 border = 0.006 * y_bounds;

	s64 x = x_start;
	s64 y = y_start;

	draw_shape_init(North_East, Icon_Col, border);

	bldr_add_c(&all.bldr, 'm');
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x); bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y); bldr_add_c(&all.bldr, ' '); bldr_add_c(&all.bldr, 'l');
	x += handle_wi;
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);
	x += mouth_wi;
	y -= 0.5 * (mouth_ht - handle_ht);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);
	y += mouth_ht;
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);
	x -= mouth_wi;
	y -= 0.5 * (mouth_ht - handle_ht);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);
	x -= handle_wi;
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, x);
	bldr_add_c(&all.bldr, ' '); bldr_add_n(&all.bldr, y);

	x += 2.5 * handle_wi + mouth_wi;
	y -= 0.5 * (handle_ht + handle_wi);
	draw_rect(x, y, handle_wi, handle_wi);

	draw_shape_finish();
	draw_commit(ctx, Mute_Overlay, (char *)Overlay_Cmd_Ass);
}

void draw_hover_pos(mpv_handle *ctx, s64 mouse_x, s64 mouse_y) {
	const s64 y_start = 0.97 * all.osd_h - Bar_Height;

	bldr_zero(&all.bldr);

	const s64 pos_percent = 100.0 * ((double)all.hover_pos / (double)all.duration);
	if (pos_percent <= 50) {
		draw_text_init(mouse_x - 5, y_start - Bar_Status_Vertical_Offset, North_West, Fg_Col, 2);
		bldr_add_s(&all.bldr, str(Left_Justified_Hover_Pos_Indicator));
		format_timestamp(all.hover_pos);
	}
	else {
		draw_text_init(mouse_x + 5, y_start - Bar_Status_Vertical_Offset, North_East, Fg_Col, 2);
		format_timestamp(all.hover_pos);
		bldr_add_s(&all.bldr, str(Right_Justified_Hover_Pos_Indicator));
	}

	draw_commit(ctx, Hover_Pos_Overlay, (char *)Overlay_Cmd_Ass);
}

boole mouse_on_bar(s64 mouse_x, s64 mouse_y, s64 *hover_pos) {
	const s64 x_bounds = all.osd_w;
	const s64 y_bounds = all.osd_h;

	const s64 width = Bar_Width_Fraction * x_bounds;
	const s64 height = Bar_Height;
	const s64 x_start = 0.5 * (x_bounds - width);
	const s64 y_start = (1.00 - Bar_Bottom_Padding_Fraction) * y_bounds - height;

	boole on_bar = Off;
	if (
		x_start <= mouse_x && mouse_x < x_start + width &&
		y_start <= mouse_y && mouse_y < y_start + height
	) {
		on_bar = On;
		const double pos_fraction = ((double)(mouse_x - x_start) / (double)width);
		*hover_pos = (s64)(pos_fraction * all.duration);
	}
	return on_bar;
}

void draw_bar(mpv_handle *ctx) {
	if (bitset_has(all.bar, Hidden_m)) return;
	const s64 x_bounds = all.osd_w;
	const s64 y_bounds = all.osd_h;

	mpv_node result;
	mpv_get_property(ctx, "percent-pos", MPV_FORMAT_DOUBLE, &result);
	const double play_fraction = result.u.double_ / 100.0;

	const s64 width = Bar_Width_Fraction * x_bounds;
	const s64 height = Bar_Height;
	const s64 x_start = 0.5 * (x_bounds - width);
	const s64 y_start = (1.00 - Bar_Bottom_Padding_Fraction) * y_bounds - height;

	bldr_zero(&all.bldr);

	const u8 cursor = Bar_Cursor_Width;

	const s64 progress_width = play_fraction * width - cursor;
	if (0 < progress_width) {
		draw_shape_init(Position_Abs, Bar_Col, Bar_Border_Width);
		draw_rect(x_start, y_start, progress_width, height);
		draw_change_color(Fg_Col);
		draw_rect(x_start, y_start, cursor, height);
		draw_change_color(Trough_Col);
		draw_rect(x_start, y_start, width - (progress_width + cursor), height);
	}
	else {
		draw_shape_init(Position_Abs, Fg_Col, Bar_Border_Width);
		draw_rect(x_start, y_start, cursor, height);
		draw_change_color(Trough_Col);
		draw_rect(x_start, y_start, width - cursor, height);
	}

	draw_shape_finish();
	draw_commit(ctx, Bar_Overlay, (char *)Overlay_Cmd_Ass);

	draw_chapter_markers(ctx, x_start, y_start, width, height);

	if (!bitset_has(all.bar, Status_Hidden_m)) {
		Draw_Bar_Status_f[all.bar_status_f_id](ctx, x_start, y_start);
	}
}

void draw_bar_status_playback(mpv_handle *ctx, const s64 x_start, const s64 y_start) {
	const s64 whole_part = all.speed;
	const s64 fractional_part = 100.0 * (all.speed - (double)whole_part);
	bldr_zero(&all.bldr);
	draw_text_init(x_start + Bar_Status_Horizontal_Offset, y_start - Bar_Status_Vertical_Offset, Position_Abs, Fg_Col, Text_Border_Width);
	if (!(whole_part == 1 && fractional_part == 0)) { 
		bldr_add_c(&all.bldr, '[');
		if (0 < whole_part)
			bldr_add_n(&all.bldr, whole_part);
		else
			bldr_add_c(&all.bldr, '0');
		if (0 < fractional_part) {
			bldr_add_c(&all.bldr, '.');
			bldr_add_n(&all.bldr, fractional_part);
		}
		bldr_add_s(&all.bldr, str("x] "));
	}
	format_timestamp(all.time_pos);
	bldr_add_s(&all.bldr, str(Time_Pos_and_Duration_Separator));
	format_timestamp(all.duration);
	if (bitset_has(all.playback, Cache_Paused_m)) bldr_add_s(&all.bldr, str(Paused_for_Cache_Status));
	else if (bitset_has(all.playback, Seeking_m)) bldr_add_s(&all.bldr, str(Seeking_Status));
	else if (bitset_has(all.playback, Idle_m) && !bitset_has(all.playback, Paused_m)) bldr_add_s(&all.bldr, str(Idling_Status));
	draw_commit(ctx, Bar_Status_Overlay, (char *)Overlay_Cmd_Ass);
};

void draw_bar_status_semantic(mpv_handle *ctx, const s64 x_start, const s64 y_start) {
	bldr_zero(&all.bldr);
	draw_text_init(x_start + Bar_Status_Horizontal_Offset, y_start - Bar_Status_Vertical_Offset, Position_Abs, Fg_Col, Text_Border_Width);
	if (1 < all.playlist_len) {
		mpv_node result;
		assert(0 <= mpv_get_property(ctx, "playlist-playing-pos", MPV_FORMAT_INT64, &result));
		const s64 playlist_pos = result.u.int64;
		bldr_add_c(&all.bldr, '[');
		if (0 < playlist_pos) bldr_add_n(&all.bldr, playlist_pos);
		else bldr_add_c(&all.bldr, '0');
		bldr_add_c(&all.bldr, '/');
		bldr_add_n(&all.bldr, all.playlist_len - 1);
		bldr_add_c(&all.bldr, ']');
		bldr_add_c(&all.bldr, ' ');
	}
	bldr_add_s(&all.bldr, all.media_title);
	draw_commit(ctx, Bar_Status_Overlay, (char *)Overlay_Cmd_Ass);
};

void draw_bar_status_chapter(mpv_handle *ctx, const s64 x_start, const s64 y_start) {
	bldr_zero(&all.bldr);
	draw_text_init(x_start + Bar_Status_Horizontal_Offset, y_start - Bar_Status_Vertical_Offset, Position_Abs, Fg_Col, Text_Border_Width);
	if (0 < all.chapter_count) {
		const s64 chapter_count = all.chapter_count;
		const s64 current = all.chapter;
		const mpv_node *chapters = all.chapters;
		if (0 <= current) {
			assert(chapters + current);
			const mpv_node_list *ch_prop = chapters[current].u.list;
			assert(!strcmp(ch_prop->keys[0], "title"));
			assert(!strcmp(ch_prop->keys[1], "time"));
			const char *title = ch_prop->values[0].u.string;
			if (1 < chapter_count) {
				bldr_add_c(&all.bldr, '[');
				if (current == 0) bldr_add_c(&all.bldr, '0');
				else bldr_add_n(&all.bldr, current);
				bldr_add_c(&all.bldr, '/');
				bldr_add_n(&all.bldr, chapter_count - 1);
				bldr_add_c(&all.bldr, ']'); bldr_add_c(&all.bldr, ' ');
			}
			s64 title_len = 0; for (; title[title_len]; title_len++);
			const str_t title_str = {.s = (char *)title, .len = title_len};
			bldr_add_s(&all.bldr, title_str);
		}
		else bldr_add_s(&all.bldr, str("No chapter selected"));
	}
	else bldr_add_s(&all.bldr, str("No chapters"));
	draw_commit(ctx, Bar_Status_Overlay, (char *)Overlay_Cmd_Ass);
};

void draw_chapter_markers(mpv_handle *ctx, s64 x_start, s64 y_start, s64 width, s64 height) {
	if (all.chapter_count <= 0) return;
	assert(all.chapters);

	const s64 count = all.chapter_count;
	const s64 current = all.chapter;
	const mpv_node *chapters = all.chapters;

	bldr_zero(&all.bldr);

	if (0 <= current) {
		assert(chapters + current);
		const mpv_node_list *ch_prop = chapters[current].u.list;
		assert(!strcmp(ch_prop->keys[0], "title"));
		assert(!strcmp(ch_prop->keys[1], "time"));
		const double time = ch_prop->values[1].u.double_;
		const double time_fraction = time / (double)all.duration;
		const s64 radius = Chapter_Marker_Radius;
		const s64 x = x_start + (s64)(time_fraction * (double)width) - radius;
		const s64 y = y_start + Bar_Height / 2;
		draw_shape_init(Position_Abs, Current_Chapter_Marker_Col, Chapter_Marker_Border_Width);
		draw_change_border_color(Current_Chapter_Marker_Border_Col);
		draw_diamond(x, y, radius);
		draw_shape_finish();
		bldr_add_c(&all.bldr, '\n');
	}

	draw_shape_init(Position_Abs, Chapter_Marker_Col, Chapter_Marker_Border_Width);
	draw_change_border_color(Chapter_Marker_Border_Col);
	for (s64 ch = 0; ch < count; ch++) {
		assert(chapters + ch);
		const mpv_node_list *ch_prop = chapters[ch].u.list;
		assert(!strcmp(ch_prop->keys[0], "title"));
		assert(!strcmp(ch_prop->keys[1], "time"));
		const double time = ch_prop->values[1].u.double_;
		const double time_fraction = time / (double)all.duration;
		const s64 radius = Chapter_Marker_Radius;
		const s64 x = x_start + (s64)(time_fraction * (double)width) - radius;
		const s64 y = y_start + Bar_Height / 2;
		if (ch != current) draw_diamond(x, y, radius);
	}
	draw_shape_finish();
	draw_commit(ctx, Chapter_Overlay, (char *)Overlay_Cmd_Ass);
};

void pause_callback(mpv_handle *ctx, mpv_event_property *prop) {
	assert(prop->format == MPV_FORMAT_FLAG);
	const boole paused_state = *(int *)prop->data;
	bitset_toggle(all.playback, Paused_m, paused_state);
	draw_pause_indicator(ctx);
	draw_mute_indicator(ctx);
	draw_bar(ctx);
}

void mute_callback(mpv_handle *ctx, mpv_event_property *prop) {
	assert(prop->format == MPV_FORMAT_FLAG);
	const boole mute_state = *(int *)prop->data;
	bitset_toggle(all.playback, Muted_m, mute_state);
	draw_mute_indicator(ctx);
}

void loop_file_callback(mpv_handle *ctx, mpv_event_property *prop) {
	assert(prop->format == MPV_FORMAT_NODE);
	mpv_node *node = (mpv_node *)prop->data;
	assert(node);
	bldr_zero(&all.bldr);
	if (node->format == MPV_FORMAT_STRING) {
		const char *loop_file = node->u.string;
		if (!strcmp(loop_file, "inf") || !strcmp(loop_file, "yes")) { bldr_add_s(&all.bldr, str("loop")); }
		else if (!strcmp(loop_file, "no")) { bldr_add_s(&all.bldr, str("not loop")); }
	}
	else if (node->format == MPV_FORMAT_FLAG) {
		const int loop_file = node->u.flag;
		if (loop_file) { bldr_add_s(&all.bldr, str("loop")); }
		else { bldr_add_s(&all.bldr, str("not loop")); }
	}
	else if (node->format == MPV_FORMAT_INT64) {
		const s64 n_times = node->u.int64;
		bldr_add_s(&all.bldr, str("loop "));
		bldr_add_n(&all.bldr, n_times);
		bldr_add_c(&all.bldr, 'x');
	}
	show_text(ctx, all.bldr.s);
}

void osd_dim_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_NODE) return;
	mpv_node_list *list = ((mpv_node *)prop->data)->u.list;
	char **keys = list->keys;
	mpv_node *values = list->values;
	assert(!strcmp(keys[0], "w"));
	assert(!strcmp(keys[1], "h"));
	all.osd_w = values[0].u.int64;
	all.osd_h = values[1].u.int64;
	draw_pause_indicator(ctx);
	draw_mute_indicator(ctx);
	draw_bar(ctx);
	if (all.show_info) draw_info(ctx);
	if (bitset_has(all.bar, Hovering_m)) {
		bldr_zero(&all.bldr);
		draw_commit(ctx, Hover_Pos_Overlay, (char *)Overlay_Cmd_None);
		bitset_off(all.bar, Hovering_m);
	}
}

void time_pos_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_NODE) return;
	assert(!strcmp(prop->name, "time-pos"));
	mpv_node *item = (mpv_node *)prop->data;
	assert(item);
	assert(item->format == MPV_FORMAT_DOUBLE);
	const s64 time_pos = item->u.double_;
	all.time_pos = time_pos; // gets truncated and extracts whole part
	draw_bar(ctx);
}

void duration_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_NODE) return;
	assert(!strcmp(prop->name, "duration"));
	mpv_node *item = (mpv_node *)prop->data;
	assert(item);
	assert(item->format == MPV_FORMAT_DOUBLE);
	const s64 duration = item->u.double_;
	all.duration = duration;
	draw_bar(ctx);
}

void speed_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_DOUBLE) return;
	const double speed = *(double *)prop->data;
	all.speed = speed;
	draw_bar(ctx);
}

void mouse_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_NODE) return;
	mpv_node_list *list = ((mpv_node *)prop->data)->u.list;
	char **keys = list->keys;
	mpv_node *values = list->values;
	assert(!strcmp(keys[0], "x"));
	assert(!strcmp(keys[1], "y"));
	assert(!strcmp(keys[2], "hover"));
	const s64 mouse_x = values[0].u.int64;
	const s64 mouse_y = values[1].u.int64;
	const boole mouse_hover = values[2].u.flag;

	if (mouse_hover && !bitset_has(all.bar, Hidden_m)) {
		s64 hover_pos = 0;
		if (mouse_on_bar(mouse_x, mouse_y, &hover_pos)) {
			all.hover_pos = hover_pos;
			draw_hover_pos(ctx, mouse_x, mouse_y);
			bitset_on(all.bar, Hovering_m);
		}
		else if (bitset_has(all.bar, Hovering_m)) {
			bldr_zero(&all.bldr);
			draw_commit(ctx, Hover_Pos_Overlay, (char *)Overlay_Cmd_None);
			bitset_off(all.bar, Hovering_m);
		}
	}
}

/*void demuxer_state_allback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_NODE) return;
	mpv_node *node = prop->data;
	assert(node);
	mpv_node_list *list = node->u.list;
	assert(list);
	char **keys = list->keys;
	assert(keys);
	mpv_node *values = list->values;
	assert(values);

	// assert(!strcmp(keys[0], "cache-end"));
	// assert(!strcmp(keys[1], "reader-pts"));
	// assert(!strcmp(keys[2], "cache-duration"));
	// assert(!strcmp(keys[3], "eof"));
	// assert(!strcmp(keys[4], "underrun"));
	// assert(!strcmp(keys[5], "idle"));
	// assert(!strcmp(keys[6], "total-bytes"));
	// assert(!strcmp(keys[7], "fw-bytes"));
	// assert(!strcmp(keys[8], "raw-input-rate"));
	// assert(!strcmp(keys[9], "debug-low-level-seeks"));
	// assert(!strcmp(keys[10], "debug-byte-level-seeks"));
	// assert(!strcmp(keys[11], "debug-ts-last"));
	// assert(!strcmp(keys[12], "ts-per-stream"));
	// assert(!strcmp(keys[13], "bof-cached"));
	// assert(!strcmp(keys[14], "eof-cached"));
	// assert(!strcmp(keys[15], "seekable-ranges"));
}
*/

void idle_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_FLAG) return;
	const boole state = *(int *)prop->data;
	bitset_toggle(all.playback, Idle_m, state);
	draw_bar(ctx);
}

void cache_pause_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_FLAG) return;
	const boole state = *(int *)prop->data;
	bitset_toggle(all.playback, Cache_Paused_m, state);
	draw_bar(ctx);
}

void chapter_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_INT64) return;
	const s64 current_chapter = *(s64 *)(prop->data);
	all.chapter = current_chapter;
}

void chapters_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_INT64) return;
	const s64 count = *(s64 *)(prop->data);
	all.chapter_count = count;
	if (count <= 0) {
		assert(0 <= mpv_unobserve_property(ctx, Chapter_Watcher));
		return;
	}
	mpv_node result;
	mpv_get_property(ctx, "chapter-list", MPV_FORMAT_NODE, &result);
	const mpv_node_list *list = result.u.list;
	assert(list);
	const s64 list_chapter_count = list->num;
	assert(list_chapter_count == all.chapter_count);
	const mpv_node *chapters = list->values;
	assert(chapters);
	all.chapters = (mpv_node *)chapters;
	mpv_get_property(ctx, "chapter", MPV_FORMAT_INT64, &result);
	const s64 current_chapter = result.u.int64;
	all.chapter = current_chapter;
	assert(0 <= mpv_observe_property(ctx, Chapter_Watcher, "chapter", MPV_FORMAT_INT64));
}

void playlist_len_callback(mpv_handle *ctx, mpv_event_property *prop) {
	assert(prop->format == MPV_FORMAT_INT64);
	const s64 count = *(s64 *)prop->data;
	all.playlist_len = count;
}

void media_title_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_STRING) return;
	const mpv_node *node = prop->data;
	const char *title = node->u.string;
	assert(title);
	s64 title_len = 0;
	for (; title[title_len]; title_len++);
	all.media_title = (str_t){.s = (char *)title, .len = title_len};
}

void bitrate_callback(mpv_handle *ctx, mpv_event_property *prop) {
	if (prop->format != MPV_FORMAT_INT64) return;
	if (all.show_info) draw_info(ctx);
}

void hide_bar(mpv_handle *ctx) {
	if (bitset_has(all.bar, Hidden_m)) {
		bitset_off(all.bar, Hidden_m);
		draw_bar(ctx);
	}
	else {
		bldr_zero(&all.bldr);
		draw_commit(ctx, Bar_Overlay, (char *)Overlay_Cmd_None);
		draw_commit(ctx, Chapter_Overlay, (char *)Overlay_Cmd_None);
		draw_commit(ctx, Bar_Status_Overlay, (char *)Overlay_Cmd_None);
		bitset_on(all.bar, Hidden_m);
		if (bitset_has(all.bar, Hovering_m)) {
			draw_commit(ctx, Hover_Pos_Overlay, (char *)Overlay_Cmd_None);
		}
	}
}

void hide_bar_status(mpv_handle *ctx) {
	if (bitset_has(all.bar, Hidden_m)) {
		bitset_off(all.bar, Hidden_m);
		bitset_off(all.bar, Status_Hidden_m);
		draw_bar(ctx);
	}
	else if (bitset_has(all.bar, Status_Hidden_m)) {
		bitset_off(all.bar, Status_Hidden_m);
		draw_bar(ctx);
	}
	else {
		bldr_zero(&all.bldr);
		draw_commit(ctx, Bar_Status_Overlay, (char *)Overlay_Cmd_None);
		bitset_on(all.bar, Status_Hidden_m);
	}
}

void proc_return(mpv_handle *ctx) {
	if (bitset_has(all.bar, Hovering_m)) {
		mpv_set_property(ctx, "time-pos", MPV_FORMAT_INT64, &all.hover_pos);
	}
	else {
		const int new_pause_state = !bitset_has(all.playback, Paused_m);
		mpv_set_property(ctx, "pause", MPV_FORMAT_FLAG, (void *)&new_pause_state);
	}
}

void yank_time_pos(mpv_handle *ctx) {
	bldr_zero(&all.bldr);
	bldr_add_n(&all.bldr, all.time_pos);
	FILE *proc_fh = popen(Yank_System_Cmd, "w");
	if (!proc_fh) {
		show_text(ctx, "Could not interact with system clipboard");
		return;
	}
	const int written = fwrite(all.bldr.s, sizeof(*all.bldr.s), all.bldr.len, proc_fh);
	if (written != (int)all.bldr.len) {
		show_text(ctx, "Could not properly write position to system clipboard");
		return;
	}
	pclose(proc_fh);
	show_text(ctx, "Sent current position to system clipboard");
}

void yank_file_path(mpv_handle *ctx) {
	mpv_node result;
	mpv_get_property(ctx, "path", MPV_FORMAT_STRING, &result);
	FILE *proc_fh = popen(Yank_System_Cmd, "w");
	if (!proc_fh) {
		show_text(ctx, "Could not interact with system clipboard");
		return;
	}
	const char *path = result.u.string;
	int len = 0;
	for (; path[len]; len++);
	const int written = fwrite(path, sizeof(*path), len, proc_fh);
	if (written != len) {
		show_text(ctx, "Could not properly write file path to system clipboard");
		return;
	}
	pclose(proc_fh);
	show_text(ctx, "Sent file path to system clipboard");
}

void paste_time_pos(mpv_handle *ctx) {
	FILE *proc_fh = popen(Paste_System_Cmd, "r");
	if (!proc_fh) {
		show_text(ctx, "Could not read system clipboard");
		return;
	}
	int pos = -1;
	const int result = fscanf(proc_fh, "%d", &pos);
	if (result != 1) {
		show_text(ctx, "Could not properly read system clipboard");
		return;
	}
	pclose(proc_fh);
	if (!(0 <= pos && pos <= all.duration)) {
		show_text(ctx, "Position fetched from clipboard not in valid range");
		return;
	}
	const s64 pos_prop = pos;
	mpv_set_property(ctx, "time-pos", MPV_FORMAT_INT64, (void *)&pos_prop);
}

void paste_filepath(mpv_handle *ctx) {
	FILE *proc_fh = popen(Paste_System_Cmd, "r");
	if (!proc_fh) {
		show_text(ctx, "Could not read system clipboard");
		return;
	}
	bldr_zero(&all.bldr);
	const int chars_read = fread(all.bldr.s, sizeof(char), Builder_Cap, proc_fh);
	if (ferror(proc_fh)) {
		show_text(ctx, "Could not properly read system clipboard");
		return;
	}
	assert(feof(proc_fh));
	pclose(proc_fh);
	if (chars_read <= 0) {
		show_text(ctx, "System clipboard path too short");
		return;
	}
	if (all.bldr.s[chars_read - 1] == '\n' || all.bldr.s[chars_read - 1] == ' ') {
		all.bldr.s[chars_read - 1] = '\0';
		for (int i = 1; i <= chars_read; i++) {
			if (all.bldr.s[chars_read - 1 - i] != '\n' && all.bldr.s[chars_read - 1 -i] != ' ') break;
			all.bldr.s[chars_read - 1 - i] = '\0';
		}
	}
	const char *load_file_behaviour[4] = {
		"insert-next",
		"insert-next-play",
		"append",
		"append-play",
	};
	const int num_args = 3;
	const mpv_node *args = (mpv_node[]) {
		{.format = MPV_FORMAT_STRING, .u.string = "loadfile"},
		{.format = MPV_FORMAT_STRING, .u.string = all.bldr.s},
		{.format = MPV_FORMAT_STRING, .u.string = (char *)load_file_behaviour[Paste_Filepath_Behaviour]},
	};
	const mpv_node_list list = {
		.num = num_args,
		.values = (mpv_node *)args,
		.keys = NULL,
	};
	mpv_node input = {
		.u.list = (mpv_node_list *)&list,
		.format = MPV_FORMAT_NODE_ARRAY,
	};
	mpv_node result;
	if (mpv_command_node(ctx, &input, &result) < 0) {
		show_text(ctx, "Could not load file from system clipboard");
		return;
	}
	show_text(ctx, "Loaded file from system clipboard");
}

void format_mem_size(s64 bytes){ 
	if (bytes < 1l << 10) {
		bldr_add_n(&all.bldr, bytes);
		bldr_add_c(&all.bldr, 'b');
		return;
	}
	s64 sub = 1l << 10; char unit = 'K';
	if      (bytes < 1l << 30) { sub = 1l << 20; unit = 'M'; }
	else if (bytes < 1l << 40) { sub = 1l << 30; unit = 'G'; }
	else if (bytes < 1l << 50) { sub = 1l << 40; unit = 'T'; }
	else { return; }
	const s64 whole_part = bytes / sub;
	const s64 fractional_part = 100 * (bytes % sub) / sub;
	if (0 < whole_part) bldr_add_n(&all.bldr, whole_part);
	else bldr_add_c(&all.bldr, '0');
	if (0 < fractional_part) {
		bldr_add_c(&all.bldr, '.');
		bldr_add_n(&all.bldr, fractional_part);
	}
	bldr_add_c(&all.bldr, ' '); bldr_add_c(&all.bldr, unit);
}

void draw_info(mpv_handle *ctx) {
	mpv_node result;
	mpv_get_property(ctx, "demuxer-via-network", MPV_FORMAT_FLAG, &result);
	const boole has_network = result.u.flag;
	mpv_get_property(ctx, "file-size", MPV_FORMAT_INT64, &result);
	const s64 byte_size = result.u.int64;
	mpv_get_property(ctx, "video-codec", MPV_FORMAT_STRING, &result);
	const char *video_fmt = result.u.string;
	mpv_get_property(ctx, "audio-codec", MPV_FORMAT_STRING, &result);
	const char *audio_fmt = result.u.string;
	mpv_get_property(ctx, "width", MPV_FORMAT_INT64, &result);
	const s64 width = result.u.int64;
	mpv_get_property(ctx, "height", MPV_FORMAT_INT64, &result);
	const s64 height = result.u.int64;
	mpv_get_property(ctx, "audio-bitrate", MPV_FORMAT_INT64, &result);
	const s64 audio_bitrate = result.u.int64;

	u8 video_fmt_len = 0;
	u8 audio_fmt_len = 0;
	for (; video_fmt[video_fmt_len]; video_fmt_len++);
	for (; audio_fmt[audio_fmt_len]; audio_fmt_len++);
	const str_t video_fmt_str = {.s = (char *)video_fmt, .len = video_fmt_len};
	const str_t audio_fmt_str = {.s = (char *)audio_fmt, .len = audio_fmt_len};

	const s64 line_ht = Info_Overlay_Line_Height;
	s64 y = 1;

	bldr_zero(&all.bldr);
	draw_text_init(1, y, Position_Abs, Fg_Col, 2);
	if (has_network) {
		bldr_add_s(&all.bldr, str("(network stream) "));
	}
	format_mem_size(byte_size);
	bldr_add_c(&all.bldr, '\n');

	y += line_ht;
	draw_text_init(1, y, Position_Abs, Fg_Col, 2);
	bldr_add_s(&all.bldr, str("video => "));
	bldr_add_n(&all.bldr, width);
	bldr_add_c(&all.bldr, 'x'); bldr_add_n(&all.bldr, height);
	bldr_add_c(&all.bldr, ','); bldr_add_c(&all.bldr, ' ');
	bldr_add_s(&all.bldr, video_fmt_str);
	bldr_add_c(&all.bldr, '\n');

	y += line_ht;
	draw_text_init(1, y, Position_Abs, Fg_Col, 2);
	bldr_add_s(&all.bldr, str("audio => "));
	if (audio_bitrate < 1000) {
		bldr_add_n(&all.bldr, audio_bitrate);
		bldr_add_s(&all.bldr, str(" bps, "));
	}
	else {
		const s64 kb = audio_bitrate / 1000;
		const s64 b  = audio_bitrate % 1000;
		bldr_add_n(&all.bldr, kb);
		if (0 < b) {
			bldr_add_c(&all.bldr, '.');
			bldr_add_n(&all.bldr, b);
		}
		bldr_add_s(&all.bldr, str(" kbps, "));
	}
	bldr_add_s(&all.bldr, audio_fmt_str);

	draw_commit(ctx, Info_Overlay, (char *)Overlay_Cmd_Ass);
}

void show_info(mpv_handle *ctx) {
	if (all.show_info) {
		bldr_zero(&all.bldr);
		draw_commit(ctx, Info_Overlay, (char *)Overlay_Cmd_None);
		all.show_info = Off;
		return;
	}
	draw_info(ctx);
	all.show_info = On;
}

void swap_bar_status(mpv_handle *ctx) {
	if (all.bar_status_f_id == Draw_Bar_Status_Playback)
		all.bar_status_f_id = Draw_Bar_Status_Semantic;
	else if (all.bar_status_f_id == Draw_Bar_Status_Semantic)
		all.bar_status_f_id = Draw_Bar_Status_Chapter;
	else if (all.bar_status_f_id == Draw_Bar_Status_Chapter)
		all.bar_status_f_id = Draw_Bar_Status_Playback;
	bitset_off(all.bar, Hidden_m);
	bitset_off(all.bar, Status_Hidden_m);
	draw_bar(ctx);
}

void vo_ready_callback(mpv_handle *ctx, mpv_event_property *prop) {
	assert(prop->format == MPV_FORMAT_FLAG);
	boole configured = *(int *)prop->data;
	if (!configured) return;

	assert(0 <= mpv_unobserve_property(ctx, Vo_Ready_Watcher));
	assert(0 <= mpv_observe_property(ctx, Osd_Dim_Watcher,     "osd-dimensions",  MPV_FORMAT_NODE));
	assert(0 <= mpv_observe_property(ctx, Pause_Watcher,       "pause",           MPV_FORMAT_FLAG));
	assert(0 <= mpv_observe_property(ctx, Mute_Watcher,        "mute",            MPV_FORMAT_FLAG));
	assert(0 <= mpv_observe_property(ctx, Loop_File_Watcher,   "loop-file",       MPV_FORMAT_NODE));
	assert(0 <= mpv_observe_property(ctx, Time_Pos_Watcher,    "time-pos",        MPV_FORMAT_NODE));
	assert(0 <= mpv_observe_property(ctx, Duration_Watcher,    "duration",        MPV_FORMAT_NODE));
	assert(0 <= mpv_observe_property(ctx, Speed_Watcher,       "speed",           MPV_FORMAT_DOUBLE));
	assert(0 <= mpv_observe_property(ctx, Mouse_Watcher,       "mouse-pos",       MPV_FORMAT_NODE));
	assert(0 <= mpv_observe_property(ctx, Idle_Watcher,        "core-idle",       MPV_FORMAT_FLAG));
	assert(0 <= mpv_observe_property(ctx, Cache_Pause_Watcher, "paused-for-cache",MPV_FORMAT_FLAG));
	assert(0 <= mpv_observe_property(ctx, Playlist_Len_Watcher,"playlist-count",  MPV_FORMAT_INT64));
	assert(0 <= mpv_observe_property(ctx, Title_Watcher,       "media-title",     MPV_FORMAT_STRING));
	assert(0 <= mpv_observe_property(ctx, Bitrate_Watcher,     "audio-bitrate",   MPV_FORMAT_INT64));
	assert(0 <= mpv_observe_property(ctx, Chapters_Watcher,    "chapters",        MPV_FORMAT_INT64));
}

int mpv_open_cplugin(mpv_handle *ctx) {
	assert(0 <= mpv_observe_property(ctx, Vo_Ready_Watcher, "vo-configured", MPV_FORMAT_FLAG));
	while (1) {
		mpv_event *event = mpv_wait_event(ctx, -1);
		if (event->event_id == MPV_EVENT_SHUTDOWN) break;
		switch(event->event_id) { case MPV_EVENT_PROPERTY_CHANGE:
			callback_f cb = NULL;
			switch(event->reply_userdata) {
			case Osd_Dim_Watcher:      cb = osd_dim_callback; break;
			case Pause_Watcher:        cb = pause_callback; break;
			case Mute_Watcher:         cb = mute_callback; break;
			case Loop_File_Watcher:    cb = loop_file_callback; break;
			case Time_Pos_Watcher:     cb = time_pos_callback; break;
			case Duration_Watcher:     cb = duration_callback; break;
			case Speed_Watcher:        cb = speed_callback; break;
			case Mouse_Watcher:        cb = mouse_callback; break;
			case Idle_Watcher:         cb = idle_callback; break;
			case Cache_Pause_Watcher:  cb = cache_pause_callback; break;
			case Chapter_Watcher:      cb = chapter_callback; break;
			case Playlist_Len_Watcher: cb = playlist_len_callback; break;
			case Title_Watcher:        cb = media_title_callback; break;
			case Bitrate_Watcher:      cb = bitrate_callback; break;
			case Chapters_Watcher:     cb = chapters_callback; break;
			case Vo_Ready_Watcher:     cb = vo_ready_callback; break;
			default: assert(0); break;
			}
			cb(ctx, (mpv_event_property *)event->data);
			break;
		case MPV_EVENT_CLIENT_MESSAGE:
			const mpv_event_client_message *client_msg = event->data;
			const int num = client_msg->num_args;
			const char **args = client_msg->args;
			if (0 == num) break;
			const char *msg = args[0];
			if      (!strcmp(msg, "toggle-bar"))             {hide_bar(ctx);}
			else if (!strcmp(msg, "toggle-bar-status"))      {hide_bar_status(ctx);}
			else if (!strcmp(msg, "sent-return"))            {proc_return(ctx);}
			else if (!strcmp(msg, "yank-current-pos"))       {yank_time_pos(ctx);}
			else if (!strcmp(msg, "yank-file-path"))         {yank_file_path(ctx);}
			else if (!strcmp(msg, "set-pos-from-clipboard")) {paste_time_pos(ctx);}
			else if (!strcmp(msg, "add-file-from-clipboard")){paste_filepath(ctx);}
			else if (!strcmp(msg, "show-info"))              {show_info(ctx);}
			else if (!strcmp(msg, "swap-bar-status"))        {swap_bar_status(ctx);}
			break;
		case MPV_EVENT_SEEK:
			bitset_on(all.playback, Seeking_m);
			draw_bar(ctx);
			break;
		case MPV_EVENT_PLAYBACK_RESTART:
			bitset_off(all.playback, Seeking_m);
			draw_bar(ctx);
			break;
		case MPV_EVENT_VIDEO_RECONFIG:
		case MPV_EVENT_AUDIO_RECONFIG:
			if (all.show_info) draw_info(ctx);
			break;
		default:
			break;
		}
	}
	return 0;
}
