# mountmenu

Lua script that uses `lsblk` to display a menu of mounted and unmounted
partitions to the user to choose from. It then uses `udevil` to mount
or unmount the partition.
