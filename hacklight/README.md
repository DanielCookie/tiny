# hacklight

- Modify system brightness.
- Query system Brightness.
- Gradually dim system brightness to its lowest level.

## Usage

```
Usage:
   hacklight set n[+|-], 0 <= n <= 96000
   hacklight set p%[+|-], 0 <= p <= 100
   hacklight dim [slow|paced|fast]
   hacklight get
```

## Building

The program assumes that the user has permission to control the system brightness.
If you don't already have that set up, modifying the `backlight.rules` file and putting
it in `/etc/udev/rules.d/`, in combination with adding the user to the video group,
might suffice. Otherwise, you must troubleshoot it yourself.

First modify `update_config.sh` by changing the backlight device directory to the correct one:
```
Linux_Dir="/sys/class/backlight/intel_backlight"
```
Then, run it by `sh update_config.sh`.

Alternatively, modify `config.ha` manually.

In `config.ha`, it is important to adjust the `Dimming_Step` constant, since
different backlight devices may have very dissimilar maximum brightnesses.
For a maximum brightness of 255, a value of 2 could be desired, but for a maximum
of 96000, a value of 250 could be desired.

To build the program, run `hare build`.
To install the program, run `install -m755 ./hacklight /usr/local/bin/` as root.
