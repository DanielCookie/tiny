Linux_Dir="/sys/class/backlight/intel_backlight"

[ -f "$Linux_Dir/max_brightness" -a -f "$Linux_Dir/brightness" ] || {
	echo Bad directory
	exit 1
}

read Max_Brightness < "$Linux_Dir/max_brightness"

sed -i -E config.ha \
	-e "s|^(def\s+Max_Brightness.+=\s*)(.*);$|\1$Max_Brightness;|" \
	-e "s|^(def\s+Linux_File.+=\s*)(.*);$|\1\"$Linux_Dir/brightness\";|"

sed -i -E backlight.rules \
	-e "s|(.*KERNEL==)\"(.*)\",(.*)\s+.+$|\1\"${Linux_Dir##*/}\",\3 $Linux_Dir/brightness|"
