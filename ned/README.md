# ned

"Name edit"

Usage:
Provide the file names to edit as arguments in the command line.

```
ned *.mp3
```

Edit all mp3 file names.
