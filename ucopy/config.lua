-- This file must be located in $XDG_CONFIG_HOME/ucopymenu
M = {}

M.devices = {
	{label = "mobile_phone", name = "Amazing Phone 9400", coupled_files = {
		{local_path = "/home/peter/dir1", remote_path = "/sdcard/dir3"},
		{local_path = "/home/peter/dir2", remote_path = "/sdcard/dir4"},
	}},
	{label = "chocolate", name = "Amazing Computer 42", coupled_files = {
		{local_path = "/home/peter/dir5", remote_path = "/home/diane/dir7"},
		{local_path = "/home/peter/dir6", remote_path = "/home/diane/dir8"},
	}},
}

return M
