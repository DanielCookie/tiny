# ucopy

`ucopy` is an `rsync` wrapper that uses sane command options and that
uses notifications to inform the user of the success or failure of the
rsync operation.

`ucopy` hardcodes deleting extraneous files from the overwritten directory
so that it completely mirrors the source directory. As an exception,
it also hardcodes ignoring _hidden_ files (those files whose name starts
with a leading dot).

`ucopymenu` is a lua script that allows the user to choose from a
user-defined list of directory couplings (one path from the local
machine and the other from the remote machine) that are specific to an
SSH host. It then performs the syncing operation, by invoking `ucopy`,
for the specified host.

In order to use the script from a keybinding or away from the terminal,
ssh must be configured to use _passwordless_ public key authentication
with the desired SSH host[s].

`ucopymenu` requires a configuration file `config.lua` to be located in
`$XDG_CONFIG_HOME/ucopymenu/`. The coupled paths that appear for each
external SSH host must be directories whose **contents** are intended
to be mirrored between one another. See the example `config.lua` that
is included.
